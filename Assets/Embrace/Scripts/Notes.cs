﻿using UnityEngine;

namespace Embrace
{
    public class Notes : MonoBehaviour
    {
        public const int SONG_NOTES_COUNT = 6;

        public static Note[] notes;

        //public static Note A;
        [SerializeField] private AudioClip sfxA;
        [SerializeField] private KeyCode keyA;

        //public static Note B;
        [SerializeField] private AudioClip sfxB;
        [SerializeField] private KeyCode keyB;
        
        //public static Note C;
        [SerializeField] private AudioClip sfxC;
        [SerializeField] private KeyCode keyC;

        //public static Note D;
        [SerializeField] private AudioClip sfxD;
        [SerializeField] private KeyCode keyD;

        //public static Note E;
        [SerializeField] private AudioClip sfxE;
        [SerializeField] private KeyCode keyE;

        private void Awake() {
            notes = new Note[5];

            notes[0] = new Note(sfxA, keyA);
            notes[1] = new Note(sfxB, keyB);
            notes[2] = new Note(sfxC, keyC);
            notes[3] = new Note(sfxD, keyD);
            notes[4] = new Note(sfxE, keyE);
        }
    }

    public struct Note
    {
        public AudioClip sfx;
        public KeyCode key;

        public Note(AudioClip sfx, KeyCode key) {
            this.sfx = sfx;
            this.key = key;
        }
    }
}
