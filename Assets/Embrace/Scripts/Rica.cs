using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Embrace
{
    [RequireComponent(typeof(Animator))]
    /// <summary>
    ///  The character that the player will control
    /// </summary>
    public class Rica : MonoBehaviour
    {
        public static Rica instance;

        public Animator anima;
        public AudioSource sfxSource;
        public GameObject songManager;

        //Seconds that needs to pass before the next note can be played
        private const float PLAY_COOLDOWN = 0.33f;
        private const float SONG_DONE_PERC = 0.75f;

        //Duration which Rica will not receive any damage
        private const float INVINCIBILITY_DUR = 0.5f;
        private const float DODGE_COOLDOWN = 0.5f;

        public bool isInvincible { get; private set; }
        public bool isDodgeReady { get; private set; }
        public bool isPlaying { get; private set; }

        private List<Note> noteBuffer;
        private float lastNoteTime;
        private Song[] songList;

        private void Awake() {
            instance = this;
        }

        private void Start() {
            anima = GetComponent<Animator>();
            sfxSource = Camera.main.GetComponent<AudioSource>();

            isInvincible = false;
            isDodgeReady = true;

            noteBuffer = new List<Note>(Notes.SONG_NOTES_COUNT);
            lastNoteTime = 0;

            getAllSongs();
        }
        
        private void getAllSongs() {
            if(songManager == null) {
                songManager = GameObject.FindGameObjectWithTag(AppData.TAG_SONG_MANAGER);
            }

            songList = songManager.GetComponents<Song>();
        }

        /// <summary>
        /// Play a note
        /// A set of notes corresponds to a song
        /// </summary>
        public IEnumerator c_PlayNote(Note noteInput) {
            isPlaying = true;

            anima.SetBool("Crouch", isPlaying);

            //Reset note buffer if next note is not played soon after
            if(Time.time - lastNoteTime >= PLAY_COOLDOWN)
                noteBuffer.Clear();

            sfxSource.PlayOneShot(noteInput.sfx);

            noteBuffer.Add(noteInput);

            yield return new WaitForSeconds(PLAY_COOLDOWN);

            isPlaying = false;

            anima.SetBool("Crouch", isPlaying);

            lastNoteTime = Time.time;

            if(noteBuffer.Count >= Notes.SONG_NOTES_COUNT)
                checkSongs();
        }

        //Check if sequence of notes played corresponds to a song
        private void checkSongs() {
            foreach(Song song in songList) {
                matchNoteToSong(song);
            }

            //Clear note buffer after processing to receive new note sequence
            noteBuffer.Clear();
        }

        //Play song sfx and its effect if it matches
        private void matchNoteToSong(Song song) {
            if(noteBuffer.Count < Notes.SONG_NOTES_COUNT)
                return;

            for(int i = 0; i < Notes.SONG_NOTES_COUNT; i++) {
                if(noteBuffer[i].key != song.sequence[i])
                    return;
            }

            StartCoroutine(c_PlaySong(song));
        }

        private IEnumerator c_PlaySong(Song song) {
            sfxSource.PlayOneShot(song.sfx);
            song.play();

            //Clear note buffer after processing to receive new note sequence
            noteBuffer.Clear();

            isPlaying = true;
            anima.SetBool("Crouch", isPlaying);

            yield return new WaitForSeconds(song.sfx.length * SONG_DONE_PERC);

            isPlaying = false;
            anima.SetBool("Crouch", isPlaying);
        }

        /// <summary>
        /// Dodge
        /// While dodging, player will not receive damage from monster hits
        /// </summary>
        // Trigger invincibility and manage dodge cooldown
        public IEnumerator c_Dodge() {
            isDodgeReady = false;
            isInvincible = true;

            float dodgeDuration = 0;
            while(dodgeDuration < INVINCIBILITY_DUR) {
                yield return new WaitForEndOfFrame();
                dodgeDuration += Time.deltaTime;
            }

            //Player will take damage again
            isInvincible = false;

            while(dodgeDuration < INVINCIBILITY_DUR + DODGE_COOLDOWN) {
                yield return new WaitForEndOfFrame();
                dodgeDuration += Time.deltaTime;
            }

            //Player can dodge again
            isDodgeReady = true;
        }
    }
}

