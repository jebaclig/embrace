﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Embrace
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Animator))]
    public abstract class Monster : MonoBehaviour
    {
        protected NavMeshAgent agent;
        protected Rigidbody body;
        protected new CapsuleCollider collider;
        protected Animator anima;

        [SerializeField] protected int maxHealth;
        [SerializeField] protected int curHealth;

        [SerializeField] protected int power;

        [SerializeField] protected int speed;

        protected void Start() {
            agent = GetComponent<NavMeshAgent>();
            body = GetComponent<Rigidbody>();
            collider = GetComponent<CapsuleCollider>();
            anima = GetComponent<Animator>();

            curHealth = maxHealth;

            
        }

        public int getMaxHealth() { return maxHealth; }

        public int getCurHealth() { return curHealth; }

        public void heal(int amount) {
            curHealth = Mathf.Clamp(curHealth + amount, 0, maxHealth);
        }

        public void damage(int amount) {
            curHealth = Mathf.Clamp(curHealth - amount, 0, maxHealth);

            if(curHealth <= 0) {
                death();
            }
        }

        protected abstract void damaged();

        protected abstract void death();

        public int getPower() { return power; }

        public int getSpeed() { return speed; }

        public void follow() {

        }

        public void attack() {

        }

        public void dodge() {

        }
    }
}
