﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Embrace
{
    public abstract class Song : MonoBehaviour
    {
        public AudioClip sfx;

        public KeyCode[] sequence;

        public abstract void play();
    }
}
