using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using Embrace;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        //Reference to main character script
        Rica rica;

        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;                   // the world-relative desired move direction, calculated from the camForward and user input.
        private bool isDodge;                      

        private void Start() {
            rica = Rica.instance;

            // get the transform of the main camera
            if(Camera.main != null) {
                m_Cam = Camera.main.transform;
            } else {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
        }


        private void Update() {
            if(!isDodge) {
                isDodge = CrossPlatformInputManager.GetButtonDown("Jump");
            }
            
            if(!rica.isPlaying && rica.isDodgeReady) {
                checkPlayNote();
            }
        }

        //Check input if any note has been pressed
        private void checkPlayNote() {
            foreach(Note note in Notes.notes) {
                if(Input.GetKeyDown(note.key))
                    StartCoroutine(rica.c_PlayNote(note));
            }
        }

        // Fixed update is called in sync with physics
        private void FixedUpdate() {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");

            // calculate move direction to pass to character
            if(m_Cam != null) {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v * m_CamForward + h * m_Cam.right;
            } else {
                // we use world-relative directions in the case of no main camera
                m_Move = v * Vector3.forward + h * Vector3.right;
            }
#if !MOBILE_INPUT
            // walk speed multiplier
            if(Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif
            // pass all parameters to the character control script
            m_Character.Move(m_Move);

            if(isDodge && m_Character.m_IsGrounded && !rica.isPlaying && rica.isDodgeReady) {
                //Trigger invincibility and cooldown
                StartCoroutine(rica.c_Dodge());
                m_Character.dodge();
            }
            isDodge = false;
        }
    }
}
